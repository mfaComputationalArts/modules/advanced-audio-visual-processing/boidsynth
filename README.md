# BOID Synth

**Advanced Audio Visual Processing - Mini Project - 2022**

**with-lasers.studio / nathan adams / nadam001@gold.ac.uk**


## Introduction




### Hardware

We use a MIDI keyboard for intput of note values - there are two options to explore

1. Use of a Roli Seaboard Rise where we can use MPE inputs to derive additional parameters
2. A standard MIDI keyboard - where we then use an additional controller to drive additional parameters

#### Parameters

* `GATE` - how long a sound plays for (i.e. like a synth gate time)
* `LIFETIME` - how long the boid will exist for after release
* `INTERVAL` - time between the boid sounding
* `PATTERN` - combined with interval, allows for additional patterns rather than just a single note
* `EMITTER` - how emitters work, are boids in a single event released from one emitter, do they emit from any etc

Specific parameter details here


### Process

When a `NOTE ON` event is recieved from the attached MIDI keyboard, we trigger the process of creating boids - which happens as follows

* The next available emitter is picked
* A boid is immediatly released with the note value, and `GATE` and `LIFETIME` values 
* Every `INTERVAL` / `PATTERN` duration a new boid is released whilst `NOTE OFF` has not been recieved




## File Structure





## Instructions









