# Wave Tables

Wave table data extracted from Noise Wavetables AAVP course work, to generate wave tables with octave noise

see [gitlab](https://gitlab.com/mfaComputationalArts/modules/advanced-audio-visual-processing/noisewavetable) for specifics
