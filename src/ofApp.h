/**

   with-lasers.studio / lxinspc

   project name: BoidSynth
   file name: ofApp.h

   created: 12/04/2022

   Main class for setting up the boid synth app
 
 
 
   Copyright © 2022 with-lasers.studio. All rights reserved.

*/

#pragma once

//openFrameworks includes
#include "ofMain.h"

//add on includes
#include "ofxDatGui.h"

//local project includes
#include "Bounds.h"
#include "Emitters.h"
#include "Boids.h"
#include "KeyboardInput.h"
#include "WaveTables.h"

class ofApp : public ofBaseApp{
  
public:
  void setup();
  void update();
  void draw();
  
  
  
private:
  
  const float SIZE = 1000.0f;

  const size_t N_EMITTERS = 5;
  
  
  Bounds _bounds;
    
  WaveTables::Ptr _waveTables;
  Boids::Ptr _boids;
  Emitters::Ptr _emitters;
  
  KeyboardInput _keyboard;
  
  ofEasyCam _camera;
  
  
  //Main program GUI
  struct {
    ofxDatGui* gui;
    ofxDatGuiLabel* boidCount;
    ofxDatGuiButton* clear;
    ofRectangle area;
  } _gui;
  
};
