#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

//  ofSetLogLevel(OF_LOG_VERBOSE);
  
  //Create bounds to use in the root octree
  _bounds.set(glm::vec3(0.0f), SIZE);
  
  //Load wave tables from files
  _waveTables = make_shared<WaveTables>(3);
  
  //Create boid system
  _boids = make_shared<Boids>();
  _boids->setup(_bounds, _waveTables);

  //Create emitters which will cause boids to be released from a specified point within the bounds
  _emitters = make_shared<Emitters>(_boids, N_EMITTERS);
 
  //Input devices
  _keyboard.setup(_emitters);
  
  ofBackground(ofColor(5));
    
  _gui.gui = new ofxDatGui(ofxDatGuiAnchor::TOP_RIGHT);
  _gui.gui->addHeader("with-lasers::boid_synth");
  _gui.gui->addFRM();
  _gui.boidCount = _gui.gui->addLabel("n boids: 000000");
  
  _gui.clear = _gui.gui->addButton("clear boids");
  _gui.clear->onButtonEvent([&](ofxDatGuiButtonEvent e){
    _boids->clear();
  });
  
  
  ofxDatGuiLog::quiet();
  
}

//--------------------------------------------------------------
void ofApp::update(){
  
  glm::vec3 guiPos = _gui.gui->getPosition();
  guiPos.y += _gui.gui->getHeight();

  _emitters->update(guiPos);
  _boids->update();

  _gui.boidCount->setLabel("n boids: " + ofToString(_boids->size(), 6, '0'));
  
  //Set gui area, which we use to turn off camera movment when mouse is over this area
  _gui.area.setPosition(_gui.gui->getPosition());
  _gui.area.setWidth(_gui.gui->getWidth());
  _gui.area.setHeight(guiPos.y);

  if (_gui.area.inside(mouseX, mouseY)) {
    _camera.disableMouseInput();
  } else {
    _camera.enableMouseInput();
  }

  
}

//--------------------------------------------------------------
void ofApp::draw(){

  _camera.begin();

  _emitters->draw();
  _boids->draw();

//  ofDrawGrid(100, 5);
  
  _camera.end();

}

