/**
 
 with-lasers.studio / lxinspc
 
 project name: BoidSynth
 file name: Emitters.h
 
 created: 01/05/2022
 
 Collection of emitters, this will be the main manager for MIDI information coming in,
 distributing notes between emitters and triggering emit status via Note On / Note Off
 
 It will also have the UI for the emitter parameters (position, direction, note distribution etc)
 
 Copyright © 2022 with-lasers.studio. All rights reserved.
 
 */

#pragma once

//openFrameworks includes
#include "ofMain.h"

//add on includes
#include "ofxDatGui.h"

//local project includes
#include "Properties.h"
#include "Emitter.h"
#include "Boids.h"
#include "Bounds.h"

class Emitters {
  
public:
  
  typedef shared_ptr<Emitters> Ptr;
  
  Emitters(Boids::Ptr boids, size_t n);
  
  void update(glm::vec3& guiPos);
  void draw();
  
  //Handlers for note on / off / all off
  void noteOn(int midiNote, int velocity);
  void noteOff(int midiNote);
  void allOff();
  
private:
  
  Boids::Ptr _boids;
  
  size_t _n;
  
  Properties::Ptr _properties;
  
  typedef vector<Emitter> _Emitters;
  _Emitters _emitters;
  
  
  //Specific note handlers
  void _noteOnUnison(int midiNote, int velocity);
  void _noteOnMono(int midiNote, int velocity);
  void _noteOnRotate(int midiNote, int velocity);
  void _noteOnPoly(int midiNote, int velocity);
  
  void _addToRoundsQueue(int midiNote, int velocity, size_t emitter, uint64_t release);
  void _updateRoundsQueue();
  size_t _getNextRoundEmitter();
  void _nextEmitter(size_t& i);             //increment emitter and check for wrap round
  
  void _noteOffUnison(int midiNote);
  void _noteOffMono(int midiNote);
  void _noteOffRotate(int midiNote);
  void _noteOffPoly(int midiNote);

  //this defines which emitter we can use
  size_t _currentEmitter = 0;
  
  //Mono round tracking
  
  struct Round {
    int midiNote;                   //Note to emit
    int velocity;                   //Velocity (Midi 0-127 style)
    size_t emitter;                 //emitter to release next
    uint64_t release;               //release time
  };
  
  typedef deque<Round> Rounds;      //we use a deque so we can push notes on the front, and pop old ones off the back
  Rounds _rounds;
  
  typedef map<int, size_t> Allocations;
  Allocations _allocations;
  
  //Poly note tracking
  Emitter::MidiNotes _polyNotes;
  
  
  
  //GUI for emitter related things
  struct {
    ofxDatGui* gui;
    ofxDatGuiMatrix* emitter;
    Emitter* current;
    ofxDatGui2dPad* posXZ;
    ofxDatGuiSlider* posY;
    ofxDatGuiSlider* elevation;
    ofxDatGuiSlider* azimuth;
    ofxDatGuiToggle* draw;
  } _gui;
  
  
  void _setupGui();
  
  vector <ofColor> _colors = {
    ofColor::aliceBlue,
    ofColor::darkorange,
    ofColor::cyan,
    ofColor::limeGreen,
    ofColor::magenta,
    ofColor::goldenRod,
    ofColor::slateBlue,
    ofColor::silver,
    ofColor::aquamarine
  };
  
};


