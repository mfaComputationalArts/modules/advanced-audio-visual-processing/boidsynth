/**
   
   project name: BoidSynth
   file name: Emitter.cpp

   created: 30/04/2022

   Copyright © 2022 with-lasers.studio. All rights reserved.

*/

#include "Emitter.h"


void Emitter::setup(glm::vec3 position, Spherical direction, Boids::Ptr boids, Properties::Ptr properties, ofColor color) {
    
  _position = position;
  _direction = direction;
  _properties = properties;
  _boids = boids;

  _color = color;
  
  _sphere.set(EMITTER_SIZE, 2);
  _setUnitDirection();
  
    
}


void Emitter::emit(int midiNote, int velocity) {
  
  switch (_properties->mode()) {
    case Properties::MONO:
    case Properties::UNISON:
      _emitUsingQueue(midiNote, velocity);
      break;
    case Properties::MONO_ROTATE:
      _emit(midiNote, ofMap(velocity, 0, 127, 0.0f, 1.0f));
      break;
    case Properties::POLYPHONIC:
      //do nothing, shouldn't have got here!
      break;
  }
}

void Emitter::_emitUsingQueue(int midiNote, int velocity) {
  
  //Set if we are emitting - this is valid for all modes except MONO_ROTATE where the Emitters class manages
  //this can calls the new notes on emitters as required
  _emitting = _properties->mode() != Properties::MONO_ROTATE;
  
  //As new event, we want to clear the _next map of boid releases
  _queue.clear();
  
  //Set the first emission time by using the delay function
  _addNoteToQueue(midiNote, velocity);
  
}

/**
 Poly version of emit
 */
void Emitter::emit(MidiNotes midiNotes) {

  if (_properties->mode() == Properties::POLYPHONIC) {
    //Only valid in polyphonic mode - others should call version with simgle midi note

    if (_emitting) {
      //don't need to start from scratch, we are just adding / removing notes
      _polyNotesChanged(midiNotes);
    } else {
      //Set that we will now be emitting, this is only cleared on a noteOff
      _emitting = true;

      //As new event, we want to clear the _next map of boid releases
      _queue.clear();
      
      //Now release boids for each of the notes recieved
      for (MidiNotes::iterator it = midiNotes.begin(); it != midiNotes.end(); ++it) {
        _addNoteToQueue(it->first, it->second);
      }
    }
  }
  
}

void Emitter::_addNoteToQueue(int midiNote, int velocity) {
  
  //Set the first emission time by using the delay function
  int delay = _properties->delay();
  
  if (delay == 0) {
    //we can emit straight awy, so do that
    _emit(midiNote, ofMap(velocity, 0, 127, 0.0f, 1.0f));
  } else {
    //Just get a variance time (as we don't want the initial main delay value)
    _queue[midiNote] = {
      .release = ofGetElapsedTimeMillis() + delay,
      .velocity = ofMap(velocity, 0, 127, 0.0f, 1.0f)
    };
  }
  
}

void Emitter::_polyNotesChanged(MidiNotes midiNotes) {
  
  //We allow for notes to be added / removed in polymode
  if (_properties->mode() == Properties::POLYPHONIC) {
    //First remove any items in the next queue
    vector<Queue::iterator> _queueRemove;
    for (Queue::iterator it = _queue.begin(); it != _queue.end(); ++it) {
      //Check to see if the queue note is in new midiNote vector sent across
      if (midiNotes.count(it->first) == 0) {
        _queueRemove.push_back(it);
      }
    }
    for (auto it : _queueRemove) {
      _queue.erase(it);
    }
    //midiNotes now contains the new set of notes to be played, so check each one
    //to see of it's in the next release queue - also we should updated velocity as this may have changed
    //through aftertouch
    for (MidiNotes::iterator it = midiNotes.begin(); it != midiNotes.end(); ++it) {
      if (_queue.count(it->first) == 0) {
        _addNoteToQueue(it->first, it->second);
      } else {
        _queue[it->first].velocity = it->second;
      }
    }
  }
}


void Emitter::emitStop() {
  
  _emitting = false;
  _queue.clear();
  
}


void Emitter::update() {
  
  //Iterate trough the map of next boids, and see if any are ready for release
  if (_emitting) {
    uint64_t now = ofGetElapsedTimeMillis();
    for (Queue::iterator it = _queue.begin(); it != _queue.end(); ++it) {
      if (it->second.release < now) {
        //we have no passed the time when this should have been released, so we emit a new
        //boid
        _emit(it->first, it->second.velocity);
      }
    }
  }
}


/**
 This is the actual creation of a boid
 */
void Emitter::_emit(int midiNote, float velocity) {
  
  //Called when we want to create a boid into the boid system
  
  for (int i = 0; i < abs(_properties->number().next()); i++) {
    
    //Convert the emitter properties into specific attributes for this boid - for example applying propery variances
    Boid::Properties p;
    
    //Midi note (handle if being called by poly emit with a specific note)
    p.midiNote = midiNote;
    
    //Set colour based on the midi note
    p.color = _boidColorFromNote(midiNote);
    
    //The following properties are things that can vary within the lifetime of the void (i.e. we want the boid to be able to change them)
    //so need to be passed in as full properties
    p.gate = _properties->gate();
    p.repeat = _properties->repeat();
    p.detune = _properties->detune();
    
    //These properties are unique to this boid, and are set by the emiter, so properties will return values for us
    p.lifetime = _properties->lifetime();
    p.mass = _properties->mass();
    
    //we need to calculate the velocity, which will use the emitter direction, it's drift
    //and the pressure (i.e. key velocity from a midi message)
    //TODO: how would we handle aftertouch here to allow the pressure to vary?
    glm::vec3 v = _setVelocity(velocity);
    
    _boids->addBoid(p, _position, v);
    
    
    if (_properties->mode() != Properties::MONO_ROTATE) {
      //Set the next release time
      _queue[midiNote] = {
        .release = ofGetElapsedTimeMillis() + _properties->delay(),
        .velocity = velocity
      };
    }

  }
    
}



void Emitter::draw() {
  
  ofPushMatrix();
  ofPushStyle();
  
  ofSetColor(_color);
  
  ofTranslate(_position);
  _sphere.drawWireframe();
  
  ofDrawLine(glm::vec2(0.0f), _unitDirection);
  
  ofPopMatrix();
  ofPopStyle();
  
  
  
}


glm::vec3 Emitter::position() {
  
  return _position;
  
}

glm::vec2 Emitter::positionXZ() {
  
  //We have to adjust as though using bounds with 0 / 0 as ofxDatGui2dPad doesn't handle these very well
  return glm::vec2(_position.x - _boids->bounds().position().x, _position.z - _boids->bounds().position().z);
  
}


float Emitter::getElevation() {
  
  return _direction.elevation;
  
}

void Emitter::setElevation(float e) {
  
  _direction.elevation = e;
  _setUnitDirection();
  
}


float Emitter::getAzimuth() {
  
  return _direction.azimuth;
  
}

void Emitter::setAzimuth(float a) {
  
  _direction.azimuth = a;
  _setUnitDirection();
  
}


void Emitter::setPositionXZ(float x, float z) {
  
  _position.x = x + _boids->bounds().position().x;
  _position.z = z + _boids->bounds().position().z;

}

void Emitter::setPositionY(float y) {
  
  _position.y = y;
  
}


ofColor Emitter::_boidColorFromNote(int midiNote) {
  
  size_t i = midiNote % 11;
  return _noteColors[i];
  
}




void Emitter::_setUnitDirection() {
  
  _unitDirection.x = DIR_LINE_LENGTH * cosf(_direction.azimuth) * sinf(_direction.elevation);
  _unitDirection.z = DIR_LINE_LENGTH * sinf(_direction.azimuth) * sinf(_direction.elevation);
  _unitDirection.y = DIR_LINE_LENGTH * cosf(_direction.elevation);
  
}


glm::vec3 Emitter::_setVelocity(float magnitude) {
  
  //same as set direction, but we slighlty tweak angles to add some variation to each release
  float _azimuth = _direction.azimuth + _properties->dAzimuth();
  float _elevation = _direction.elevation + _properties->dElevation();
  
  magnitude *= _properties->velocity();
  
  glm::vec3 v;
  
  v.x = magnitude * cosf(_azimuth) * sinf(_elevation);
  v.z = magnitude * sinf(_azimuth) * sinf(_elevation);
  v.z = magnitude * cosf(_azimuth);
  
  return v;
  
}
