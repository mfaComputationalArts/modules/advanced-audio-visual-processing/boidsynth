/**
   
   project name: BoidSynth
   file name: Emitters.cpp

   created: 01/05/2022

   Copyright © 2022 with-lasers.studio. All rights reserved.

*/

#include "Emitters.h"

Emitters::Emitters(Boids::Ptr boids, size_t n) {
  
  
  _boids = boids;

  _n = n;

  //Create properties
  _properties = make_shared<Properties>();
  
  _emitters.resize(_n);

  _setupGui();
  
  
  int i = 0;
  for (_Emitters::iterator it = _emitters.begin(); it != _emitters.end(); ++it, i++) {
    
    //Set a random direction
    Emitter::Spherical direction;
    direction.elevation = ofRandom(0, TWO_PI);
    direction.azimuth = ofRandom(0, PI);
    
    //Set up the emitter
    it->setup(_boids->bounds().randomPositionWithin(), direction, _boids, _properties, _colors[i]);
      
  }
  
}





void Emitters::update(glm::vec3& guiPos) {
  
  //Lock GUI to position supplied in the update call (i.e. the foot of the parent)
  //and return the foot position of this gui item for any other items
  _gui.gui->setPosition(guiPos.x, guiPos.y);
  guiPos.y += _gui.gui->getHeight();
  
  _properties->update(guiPos);
  
  if (_properties->mode() == Properties::MONO_ROTATE) {
    //we do all the emission from here directly, we don't use the individual emitters update functions
    _updateRoundsQueue();
  } else {
    //Let each emitter check for it's own updated
    for (_Emitters::iterator it = _emitters.begin(); it != _emitters.end(); ++it) {
      it->update();
    }
  }
  
  
}



void Emitters::draw() {

  //Only draw emitters if set in the scene
  
  if (_gui.draw->getChecked()) {
    for (_Emitters::iterator it = _emitters.begin(); it != _emitters.end(); ++it) {
      it->draw();
    }
  }

}


#pragma mark - Note Handling

void Emitters::noteOn(int midiNote, int velocity) {
  
  //We need to direct the recieved note to one or more emitters - which will depend on the mode
  switch (_properties->mode()) {
    case Properties::UNISON:
      _noteOnUnison(midiNote, velocity);
      break;
    case Properties::MONO:
      _noteOnMono(midiNote, velocity);
      break;
    case Properties::MONO_ROTATE:
      _noteOnRotate(midiNote, velocity);
      break;
    case Properties::POLYPHONIC:
      _noteOnPoly(midiNote, velocity);
      break;
  }
  
}

void Emitters::noteOff(int midiNote) {
  
  //We need to direct the recieved note to one or more emitters - which will depend on the mode
  switch (_properties->mode()) {
    case Properties::UNISON:
      _noteOffUnison(midiNote);
      break;
    case Properties::MONO:
      _noteOffMono(midiNote);
      break;
    case Properties::MONO_ROTATE:
      _noteOffRotate(midiNote);
      break;
    case Properties::POLYPHONIC:
      _noteOffPoly(midiNote);
      break;
  }
  
}

#pragma mark Mode: UNISON

void Emitters::_noteOnUnison(int midiNote, int velocity) {
  
  //this one is easy - we send this note and velocity to all emitters
  for (_Emitters::iterator it = _emitters.begin(); it != _emitters.end(); ++it) {
    it->emit(midiNote, velocity);
  }
  
}

void Emitters::_noteOffUnison(int midiNote) {
  
  //this one is easy - we send this note and velocity to all emitters
  for (_Emitters::iterator it = _emitters.begin(); it != _emitters.end(); ++it) {
    it->emitStop();
  }
  
}

#pragma mark Mode: MONO

void Emitters::_noteOnMono(int midiNote, int velocity) {
  
  //we need to keep track of which emitters are free, and either use the first free one - or note steel from the last one
  //the info we need is - currently allocated emitters
  
  //we also want to add some variety by rotating the start point - so that for a sequence of notes where note off occurs between
  //each one, then we move to the next one
  
  
  _allocations[midiNote] = _currentEmitter;
  _emitters[_currentEmitter].emit(midiNote, velocity);

//  cout << "current emitter " << _currentEmitter << endl;
  
  _currentEmitter++;
  if (_currentEmitter >= _n) {
    _currentEmitter = 0;
  }
  
}

void Emitters::_noteOffMono(int midiNote) {
  
  if (_allocations.count(midiNote) > 0) {
    _emitters[_allocations[midiNote]].emitStop();
    _allocations.erase(midiNote);
  }
  
}


#pragma mark Mode: MONO_ROTATE

void Emitters::_noteOnRotate(int midiNote, int velocity) {
  
  //This is just like the emitter process - but we want to rotate the queue round the emitters
  //i.e. if you play note C4 and nothing else is playying at this point in time - then it should
  //start on emitter 0, then move to emitter 1 on it's next release, then … then n, then back to 0
  
  //TODO: we could (and probabaly should) abstract the queue functionality into it's own class for this and Emitterso we don't have to repeat it here (although we might need some differences, which is one reason for not doing straight away)
  
  //Actually it is different - as we want to store same number of notes as emitters, as we will only emit one note per emitter
  //and then cycle round them
  
  //TODO: there are options for all sorts of funky things here like NI's Rounds Plugin - but lets not get carried away :)
  
  //we have a new note down (we're assuming that the input devices are handling this OK - though we are double checking in other noteOn
  //functions. So we need to add it to the next available slot.
  
  //We always push a new entry on the front, first check though if we emit with no delay (variance can change this)
  uint64_t delay = _properties->delay();
  
  size_t i = _getNextRoundEmitter(); //Note this will pop_back if the queue is full, to get us the emitter value
  if (delay == 0) {
    //emit directly, so we need to get the appropriate emitter
    _emitters[i].emit(midiNote, velocity);
    _nextEmitter(i);
    delay = _properties->delay();
  }
  _addToRoundsQueue(midiNote, velocity, i, delay);
  
}

void Emitters::_addToRoundsQueue(int midiNote, int velocity, size_t emitter, uint64_t delay) {
 
  //Always push to the front of the deque - and if necessary we will pop off the back of it, once we have filled all the slots
  _rounds.push_front({
    .midiNote = midiNote,
    .velocity = velocity,
    .emitter = emitter,
    .release = ofGetElapsedTimeMillis() + delay
  });
  //However we don't need to check for the pop_back, as emitter allocation does this for us.
  //Currently not sure if I prefer this separated approiach, but it does save an extra check
  
}


void Emitters::_updateRoundsQueue() {
  
  //We call this when in MONO_ROTATE mode, to see if there are any boids which need emitting, and if so emit them and
  //update the rounds queue
  uint64_t now = ofGetElapsedTimeMillis();
  for (Rounds::iterator it = _rounds.begin(); it != _rounds.end(); ++it) {
    //Check for release required
    if (it->release < now) {
      //emit the boid for this midiNote and velocity
      _emitters[it->emitter].emit(it->midiNote, it->velocity);
      //get next emitter
      _nextEmitter(it->emitter);
      //and update the release time
      it->release = now + _properties->delay();
    }
  }
  
}

size_t Emitters::_getNextRoundEmitter() {
  
  size_t i;
  //We need to find which is the first emitter free (as we are currently cycling round them with notes)
  //two easy cases first though
  
  if (_rounds.size() == 0) {
    i = 0;
  } else if (_rounds.size() == _n) {
    //all slots in the rounds queue are full, this means we are going to pop the last item off in the add - so we can
    //use the emitter allocated to that. rather than check again later - lets pop_back here anyway
    i = _rounds.back().emitter;
    _rounds.pop_back();
  } else {
    //Complex scxenario, we need to find the first value out of all emitters that is not in use
    vector<size_t> emitters = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };             //ULP HARD CODED! (could be better)
    for (Rounds::iterator it = _rounds.begin(); it != _rounds.end(); ++it) {
      //remove emitetr from the list
      vector<size_t>::iterator eit = find_if(emitters.begin(), emitters.end(), [&](const size_t i){
        return i ==it->emitter;
      });
      if (eit != emitters.end()) {
        emitters.erase(eit);
      }
    }
    //emitter is the first item left
    i = emitters.front();
  }
  return i;
}

void Emitters::_nextEmitter(size_t& i) {
  
  //We are just incrementing the emitter at this stage
  //TODO: we could thing about other patterns of behaviour - e.g. decrement, ping-pong, random
  i++;
  if (i >= _n) {
    i = 0;
  }
  
}

void Emitters::_noteOffRotate(int midiNote) {
  
  //This is fairly straight forward, we just need to remove the note from the rounds deque
  Rounds::iterator it = find_if(_rounds.begin(), _rounds.end(), [&](const Round r) {
    return r.midiNote == midiNote;
  });
  
  if (it != _rounds.end()) {
    _rounds.erase(it);
  }
  
  
}


#pragma mark Mode: POLY

void Emitters::_noteOnPoly(int midiNote, int velocity) {
  
  //if we have no current notes, then we call emitPoly, othewwise we call polyNotes changed
  _polyNotes[midiNote] = velocity;
  
  for (_Emitters::iterator it = _emitters.begin(); it != _emitters.end(); ++it) {
    it->emit(_polyNotes);
  }
  
}

void Emitters::_noteOffPoly(int midiNote) {
  
  //remove note from map with poly notes
  _polyNotes.erase(midiNote);
  
  //either update poly notes, or stop everything if no notes left
  for (_Emitters::iterator it = _emitters.begin(); it != _emitters.end(); ++it) {
    if (_polyNotes.size() == 0) {
      it->emitStop();
    } else {
      it->emit(_polyNotes);
    }
  }
  
}














void Emitters::allOff() {
  
  
  
}


void Emitters::_setupGui() {
  
  _gui.gui = new ofxDatGui(ofxDatGuiAnchor::TOP_RIGHT);
  _gui.gui->addHeader("Emitters");
  
  //selector emitter
  _gui.emitter = _gui.gui->addMatrix("Emitter", (int)_n, true);
  _gui.emitter->setRadioMode(true);
  
  _gui.current = &_emitters[0];
  
  //emitter properties
  _gui.posXZ = _gui.gui->add2dPad("XZ position", _boids->bounds().xzRect());
  _gui.posXZ->setPoint(glm::vec2(_gui.current->position().x, _gui.current->position().z));
  float minMax = _boids->bounds().size().y / 2.0f;
  _gui.posY = _gui.gui->addSlider("Y position", -minMax, minMax, _gui.current->position().y);
  _gui.elevation = _gui.gui->addSlider("elevation", 0.0f, PI);
  _gui.azimuth = _gui.gui->addSlider("azimuth", 0.0f, TWO_PI);
  
  _gui.draw = _gui.gui->addToggle("draw");
  _gui.draw->setChecked(true);
  
  
  
  //Handlers
  
  bool _matrixUpdate = false;
  
  _gui.emitter->onMatrixEvent([&](ofxDatGuiMatrixEvent e) {
    //Set GUI elements to the newly selected emitter
    _matrixUpdate = true;
    _gui.current = &_emitters[e.child];
    _gui.posXZ->setPoint(_gui.current->positionXZ());
    _gui.posY->setValue(_gui.current->position().y);
    _gui.elevation->setValue(_gui.current->getElevation());
    _gui.azimuth->setValue(_gui.current->getAzimuth());
    _matrixUpdate = false;
  });
 
  //Handle 2D pad input
  _gui.posXZ->on2dPadEvent([&](ofxDatGui2dPadEvent e){
    //update the emitter XZ position
    if (!_matrixUpdate) {
      _gui.current->setPositionXZ(e.x, e.y);
    }
  });
  
  //Handle slider input for position Y
  _gui.posY->onSliderEvent([&](ofxDatGuiSliderEvent e){
    //update the emitter Z position
    if (!_matrixUpdate) {
      _gui.current->setPositionY(e.value);
    }
  });
  
  //Handle Elevation changes
  _gui.elevation->onSliderEvent([&](ofxDatGuiSliderEvent e) {
    if (!_matrixUpdate) {
      _gui.current->setElevation(e.value);
    }
  });

  //Handle Azimuth changes
  _gui.azimuth->onSliderEvent([&](ofxDatGuiSliderEvent e) {
    if (!_matrixUpdate) {
      _gui.current->setAzimuth(e.value);
    }
  });

  
}
