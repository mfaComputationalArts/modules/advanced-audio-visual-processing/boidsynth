/**

   with-lasers.studio / lxinspc

   project name: BoidSynth
   file name: Emitter.h

   created: 30/04/2022

   Emitter - point in space which can trigger release of boids

   Copyright © 2022 with-lasers.studio. All rights reserved.

*/

#pragma once

//openFrameworks includes
#include "ofMain.h"

//add on includes


//local project includes
#include "Property.h"
#include "Properties.h"
#include "Boids.h"

class Emitter {
  
public:
  
  struct MidiNote {
    int note;
    int velocity;
  };
  
  typedef map<int, float> MidiNotes;
  
  struct Spherical {
    float elevation = 0.0f;
    float azimuth = 0.0f;
    float radius = 1.0f;
  };
  
  void setup(glm::vec3 position, Spherical direction, Boids::Ptr boids, Properties::Ptr properties, ofColor color);
  
  //Start emitting
  void emit(int midiNote, int velocity);                      //Used for UNISON, MONO and MONO_ROTATE starts
  void emit(MidiNotes midiNotes);             //Use for polyphonic start
  
  //Stop emitting
  void emitStop();
  
  //The update function checks to see if we should be emitting a boid, and if one is due down to the delay
  void update();
  
  //Draw the emitter if required
  void draw();
  
  //Accessor methods
  glm::vec3 position();
  glm::vec2 positionXZ();

  void setPositionXZ(float x, float z);
  void setPositionY(float y);
  
  float getElevation();
  float getAzimuth();

  void setElevation(float e);
  void setAzimuth(float a);
  
  
  
private:
  
  //Position in space
  glm::vec3 _position;
  
  //direction boids are emitted in, and small variarce in angle
  Spherical _direction;
  
  Boids::Ptr _boids;
  
  //Emitter status
  bool _emitting;
  Properties::Ptr _properties;
  
  void _emitUsingQueue(int midiNote, int velocity);
  void _emit(int midiNote = -1, float velocity = 1.0f);
  
  void _polyNotesChanged(MidiNotes midiNotes);               //Used when polyphonic has started and is running, and we need to change notes (added and removed)

  
  //Queue data - this contains a vector of midi notes as a map, which have current velocity and
  //when the next boid shoul;d be released
  struct Item {
    float velocity;                   //We will normalise from the recieved 0-127 to 0.0f to 1.0f
    uint64_t release;
  };
  
  typedef map<int, Item> Queue;
  Queue _queue;
  

  void _addNoteToQueue(int midiNote, int velocity);

  
  ofColor _boidColorFromNote(int midiNote);
  
  
  //Colors for each tone (so we have 11 colours available
  //TODO: there could be some much better ways of allocating colour, for example we
  //could use scales / keys, then assign harmonious / clashing colours accordingly
  //this however will do as a random first pass selection
  vector<ofColor> _noteColors = {
    ofColor::aliceBlue,
    ofColor::aquamarine,
    ofColor::blueViolet,
    ofColor::chartreuse,
    ofColor::cornflowerBlue,
    ofColor::cyan,
    ofColor::darkOrchid,
    ofColor::darkorange,
    ofColor::deepPink,
    ofColor::gold,
    ofColor::greenYellow
  };
  
  
  ofColor _color;
  
  //Draw emitter as sphere for debug
  const float EMITTER_SIZE = 10.0f;
  ofIcoSpherePrimitive _sphere;
  //Draw line to show direction for things to be emitted
  const float DIR_LINE_LENGTH = 60.0f;
  glm::vec3 _unitDirection;
  
  void _setUnitDirection();
  glm::vec3 _setVelocity(float magnitude);
  
};


