/**
   
   project name: BoidSynth
   file name: Properties.cpp

   created: 02/05/2022

   Copyright © 2022 with-lasers.studio. All rights reserved.

*/

#include "Properties.h"

Properties::Properties() {
  
  //Create the GUI
  _gui.gui = new ofxDatGui(ofxDatGuiAnchor::TOP_RIGHT);
  _gui.gui->addHeader("properties");
  
  //Setup default values
  _mode = MONO;
  _gui.mode = _gui.gui->addButton(_getModeLabelText());
  //TODO: handler
  _gui.mode->onButtonEvent([=](ofxDatGuiButtonEvent e){
    //cycle though modes
    int i = static_cast<int>(_mode);
    i++;
    i %= 4;
    _mode = static_cast<Mode>(i);
    _gui.mode->setLabel(_getModeLabelText());
  });
  
  
  //gate
  _gate.set(300, 50);
  _gate.addGui(_gui.gui, "gate", 50, 500, 0);
  
  //repeat
  _repeat.set(200, 30);
  _repeat.addGui(_gui.gui, "repeat", 200, 1000, 0);
  
  //delay
  _delay.set(100, 0);
  _delay.addGui(_gui.gui, "delay", 0, 3000, 0);
  
  //velocity
  _velocity.set(3.0f, 0.3f);
  _velocity.addGui(_gui.gui, "velocity", 0.5f, 30.0f, 2);
  
  //drift
  _drift.azimuth.set(0.01, 0.001);
  _drift.azimuth.addGui(_gui.gui, "drift", 0.0f, 0.1f, 3);
  _drift.elevation.set(0.01, 0.001);
  _drift.elevation.addGui(_gui.gui, "drift", 0.0f, 0.1f, 3);

  //lifetime
  _lifetime.set(7000, 100);
  _lifetime.addGui(_gui.gui, "lifetime", 500, 20000, 0);
  
  //detune
  _detune.set(0, 0);
  _detune.addGui(_gui.gui, "detune", 0, 24, 0);
  
  //mass
  _mass.set(1.0f, 0.2f);
  _mass.addGui(_gui.gui, "mass", 0.3f, 2.5f, 2);
  
  //number
  _number.set(3, 2);
  _number.addGui(_gui.gui, "number to emit", 1, 30, 0);
  
}


void Properties::update(glm::vec3& guiPos) {
  
  //Position the gui relative to the parent gui built in
  _gui.gui->setPosition(guiPos.x, guiPos.y);
  guiPos.y += _gui.gui->getHeight();
  
}
                                        
#pragma mark - Accessors

Properties::Mode Properties::mode() {
  return _mode;
}

Property<int> Properties::gate() {
  return _gate;
}

Property<int> Properties::repeat() {
  return _repeat;
}

int Properties::delay(bool varOnly) {
  //slight change to others, as for first emit we want a variance only, not value varied by variance
  return varOnly ? _delay.nextVariance() : _delay.next();
}

float Properties::velocity() {
  return _velocity.next();
}

float Properties::dAzimuth() {
  return _drift.azimuth.next();
}

float Properties::dElevation() {
  return _drift.elevation.next();
}

int Properties::lifetime() {
  return _lifetime.next();
}

int Properties::detune() {
  return _detune.next();
}

float Properties::mass() {
  return _mass.next();
}

Property<int> Properties::number() {
  return _number;
}

string Properties::_getModeLabelText() {
  
  string label;
  
  string _modes[4] = {
    "UNISON",
    "MONO",
    "ROTATE",
    "POLY"
  };
  
  int c = static_cast<int>(_mode);
  
  for (int i = 0; i < 4; i++) {
    if (i == c) {
      label += "[" + _modes[i] + "] ";
    } else {
      label += _modes[i] + " ";
    }
  }
  
  return label;
  
}
