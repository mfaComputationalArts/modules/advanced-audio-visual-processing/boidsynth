/**

   with-lasers.studio / lxinspc

   project name: BoidSynth
   file name: Boid.h

   created: 11/04/2022

   Individual implmentation of a boid, collected together in Boids.h / Boids.cpp
 
   Based on the work by Craig Reynolds, where a boid has three primary steering behaviours, namely
 
   * Seperation - steer to avoid crowding local flockmates
   * Alignment -
   * Cohesion

   Copyright © 2022 with-lasers.studio. All rights reserved.

*/

#pragma once

//openFrameworks includes
#include "ofMain.h"

//add on includes

//local project includes
#include "Entity.h"
#include "Bounds.h"
#include "Property.h"
#include "WaveTableOsc.h"
#include "WaveTables.h"
#include "HOAFunctions.h"

class Boid : public Entity {
  
public:
  
  typedef shared_ptr<Boid> Ptr;
  typedef vector<Ptr> Boids;
  
  //TODO: abstract this like we have done with Emitter properties so it can have it's own UI
  struct Weights {
    float seperation = 3.5f;
    float distance = 10000.0f;
    float align = 7.5f;
    float cohesion = 7.0f;
    float neighbourhood = 10000.0f;
  };
  
  typedef shared_ptr<Weights> WeightsPtr;
  
  struct Properties {
    //General BOID type properties, as per Craig Reynoolds
    float peripheral = HALF_PI;
    //Specific properties for this implmentation
    int midiNote;
    Property<int> gate;
    Property<int> repeat;
    int detune;
    uint64_t lifetime;
    ofFloatColor color;
    float mass = 1.0f;
  };
  
  //Creation function
  Boid(size_t id, Properties p, WeightsPtr w, glm::vec3 pos, glm::vec3 velocity, Bounds bounds, WaveTableOsc::WaveTablePtr waveTable, double sampleRate);
  
  //Revive - reuse an existing boid and bring back to life in a new location, with new properties
  void revive(Properties p, glm::vec3 pos, glm::vec3 velocity, WaveTableOsc::WaveTablePtr waveTable);
  
  //we have to implmement an update function, for the abstract class Entity
  void update(Entity::Entities& entities) override;
  
  
  void draw() override;
  
  //Audio playback for the boid
  void fillBuffer(ofSoundBuffer& output, size_t size, uint64_t length, float gain);
  
private:
  
  //TRACE_ID can be set to an ID, and then values can be reported fot it opn each value
  //set to -1 to have no tracing
  const int TRACE_ID = -1;
  
  Properties _props;
  WeightsPtr _weights;
  
  Bounds _bounds;
  
  //Audio things
  double _sampleRate;
  WaveTableOsc _wtOsc;
  
  double _frequency;
  double _midiNoteToFrequency(int note);
  
  HOA _hoa;
  
  size_t _usToSamples(uint64_t us);

  
  //Timing related things
  //Timestamp that boid ends at
  uint64_t _endLife;
  //Play status
  struct {
    bool open;                   //Is currently playing
    uint64_t next;                  //Timestamp when next swicth occurs - in µs not ms
  } _gate;
  
  void _setPlaying();
  void _setNotPlaying();
  
  //Flocking
  glm::vec3 _flock(Entity::Entities& e);
  
  bool _boidIsVisible(Entity::Ptr b);
  
  struct _FlockAggregate {
    glm::vec3 sum = glm::vec3(0.0f);
    size_t n = 0;
  };
  
  void _accumulateAlignment(Entity::Ptr b, _FlockAggregate& a);
  void _accumulateCohesion(Entity::Ptr b, _FlockAggregate& a);
  void _accumulateSeparation(Entity::Ptr b, _FlockAggregate& a);

  glm::vec3 _calculateAlignment(const _FlockAggregate& a);
  glm::vec3 _calculateCohesion(const _FlockAggregate& a);
  glm::vec3 _calculateSeparation(const _FlockAggregate& a);

  //Force application
  glm::vec3 _applyForce(glm::vec3 f);
  
  //Velocity
  glm::vec3 _limit(glm::vec3 v, float max);
  
  const float MAX_FORCE = 2.5f;
  const float MAX_VELOCITY = 1.5f;
  
  glm::vec3 _tForce;
  
  //test Out of Bounds - to be replaced by steering force to avoid bounds
  bool _testOOB();
  
  
  
  //Boid Drawing
  const float BOID_LENGTH = 5.0f;
  const float BOID_HEIGHT = 0.689f;
  const float BOID_BACK = 6.0f;
  const float BOID_WIDTH = 1.67f;
  
  ofMesh _mesh;
  void _makeMesh();
  void _animateMesh();
  
};




