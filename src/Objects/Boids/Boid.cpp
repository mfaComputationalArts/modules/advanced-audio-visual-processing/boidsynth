/**
 
 project name: BoidSynth
 file name: Boid.cpp
 
 created: 11/04/2022
 
 Copyright © 2022 with-lasers.studio. All rights reserved.
 
 */

#include "Boid.h"

Boid::Boid(size_t id, Properties p, WeightsPtr w, glm::vec3 pos, glm::vec3 velocity, Bounds bounds, WaveTableOsc::WaveTablePtr waveTable, double sampleRate) {
  
  _id = id;
  
  _props = p;
  _weights = w;
  _current.position = pos;
  _current.velocity = velocity;
  _bounds = bounds;

  _wtOsc.setup(sampleRate);
  _wtOsc.set(waveTable, false);
  _sampleRate = sampleRate;
  
  //Need to set frequency based on midi note and detune values passed across (although we might skip detune for the moment)
  _frequency = _midiNoteToFrequency(_props.midiNote);
  
  _makeMesh();
  
  //Timing related things
  //First the time at which this should no longer exist - based on the lifetime passed in
  _endLife = ofGetElapsedTimeMillis() + _props.lifetime;
  
  //Second the play status - the boid should play for GATE time and then wait for REPEAT time before playing again
  _setPlaying();
  
  //TODO: look at this, some early test code that caused issues when removed :(
  _tForce = glm::vec3(ofRandom(-1.0f, 1.0f), ofRandom(-1.0f, 1.0f), ofRandom(-1.0f, 1.0f));
  _tForce *= 0.1f;
  
  _current.velocity += _limit(_applyForce(_tForce), MAX_VELOCITY);
  
  
}

void Boid::revive(Properties p, glm::vec3 pos, glm::vec3 velocity, WaveTableOsc::WaveTablePtr waveTable) {
  
  _props = p;
  _current.position = pos;
  _current.velocity = velocity;
  
  _wtOsc.set(waveTable, false);

  //Most important thing - reset alive to true!
  _alive = true;
  
  //Timing related things
  //First the time at which this should no longer exist - based on the lifetime passed in
  _endLife = ofGetElapsedTimeMillis() + _props.lifetime;
  
  //Second the play status - the boid should play for GATE time and then wait for REPEAT time before playing again
  //note as the colour may well have changed, this will change the mesh colours for us anyway…
  _setPlaying();

  //TODO: look at this, some early test code that caused issues when removed :(
  _tForce = glm::vec3(ofRandom(-1.0f, 1.0f), ofRandom(-1.0f, 1.0f), ofRandom(-1.0f, 1.0f));
  _tForce *= 0.1f;
  
  _current.velocity += _limit(_applyForce(_tForce), MAX_VELOCITY);
  
}





#pragma mark - Update

void Boid::update(Entity::Entities& e) {
  
  //Using basing Euler integration to take a steering force, convert that into an acceleration, which can be applied
  //to current velocity, and therfore position
  
  //For the future it might be interesting to look at verlet integration, but not today
  
  if (_alive) {
    
    glm::vec3 steerForce = _limit(_flock(e), MAX_FORCE);
    glm::vec3 acceleration = _applyForce(steerForce);
    
    
    //So that we work with consistent positions during the pass over all entities (and allow ourselves to protect data between threads)
    _updated.velocity = _limit(_current.velocity + acceleration, MAX_VELOCITY);
    _updated.position = _current.position + _updated.velocity;
    
    //set heading after update
    _updated.heading = atan2f(_updated.velocity.z, _updated.velocity.x);
    
    if (_id == TRACE_ID) {
      cout << "Boid::TRACE_ID::" << ofToString(TRACE_ID) << " steer: " << steerForce << " acc: " << acceleration << " v: " << _updated.velocity << " pos: " << _updated.position << " heading: " << _updated.heading << endl;
    }

    //Check for play status
    if (ofGetElapsedTimeMicros() > _gate.next) {
      if (_gate.open) {
        _setNotPlaying();
      } else {
        _setPlaying();
      }
    }
    
    //Test to see if still alive - Out of Bounds (pre steering force against bounds), and end life
    bool death = ofGetElapsedTimeMillis() > _endLife || _testOOB();
    
    //Might as well resolve this limbo!
    if (_alive && death) {
      _death = true;                //Death is a one off event, we use to record the index being released
      _alive = false;               //Alive is the permamnt status we use to check process on things
    } else {
      //Animate mesh based on the timing loop
      _animateMesh();
    }
  }
}

#pragma mark - Play Status

void Boid::_setPlaying() {

  _wtOsc.resetPhase();

  _gate.open = true;
  _gate.next = ofGetElapsedTimeMicros() + (_props.gate.next() * 1000);
  
  //set colour alpha to 100%
  _mesh.setColorForIndices(0, 6, _props.color);
}

void Boid::_setNotPlaying() {
  
  _gate.open = false;
  _gate.next = ofGetElapsedTimeMicros() + (_props.repeat.next() * 1000);
  
  //set colour alpha to
  ofColor _c = _props.color;
  _c.a = 60;
  _mesh.setColorForIndices(0, 6, _c);
  
  _wtOsc.resetPhase();

}


void Boid::draw() {
  
  if (_alive) {
    ofPushMatrix();
    ofTranslate(_current.position);
    ofRotateYRad(_current.heading);
    _mesh.drawWireframe();
    ofPopMatrix();
  }
  
}

#pragma mark - Flocking

glm::vec3 Boid::_flock(Entity::Entities& e) {
  
  //The flock function needs to calculate the steering force that will be applied to the boid as a result of interaction with
  //it's neighbours, and the environment
  
  glm::vec3 force;
  
  //Loop though all the entities that exist alongside this boid, and grab any required data for them
  
  _FlockAggregate _alignmentAggregate;
  _FlockAggregate _cohesionAggregate;
  _FlockAggregate _separationAggregate;
  
  
  for (Entity::Entities::iterator it = e.begin(); it != e.end(); ++it) {
    //We don't want to include this boid in the calculation - just the others in the swarm
    if ((*it)->id() != _id) {
      //We want to check if the boid is in sight of this boid
      if (_boidIsVisible((*it))) {
        //Alignment, seperation and cohesion all require an aggreate of information from the boids to process, so we will acculuate
        //this and then process once we have all todays
        _accumulateAlignment((*it), _alignmentAggregate);
        _accumulateCohesion((*it), _cohesionAggregate);
        _accumulateSeparation((*it), _separationAggregate);
      }
    }
  }
  
  //Having looped through all boids, we can use their aggregate data to create the overall steering force which is applied
  force += _calculateAlignment(_alignmentAggregate);
  force += _calculateCohesion(_cohesionAggregate);
  force += _calculateSeparation(_separationAggregate);
  
  return force;
  
}

#pragma mark Flocking Forces

void Boid::_accumulateAlignment(Entity::Ptr b, _FlockAggregate &a) {
  
  a.sum += b->current().velocity;
  a.n++;
  
}

glm::vec3 Boid::_calculateAlignment(const _FlockAggregate &a) {
  
  glm::vec3 force = glm::vec3(0.0f);
  glm::vec3 align = glm::vec3(0.0f);
  
  
  if (a.n > 0) {
    align = a.sum / (float)a.n;
    align = glm::normalize(align);
    align *= MAX_VELOCITY;
    //Calculate the steering force
    force = _limit(_current.velocity - align, MAX_FORCE);
  }
  
  if (_id == TRACE_ID) {
    cout << "Boid::TRACE_ID::" << ofToString(_id) << " sum: " << a.sum << " n: " << a.n  << " align: " << align << " force: " << force << endl;
  }
  
  
  return force;
  
}

void Boid::_accumulateCohesion(Entity::Ptr b, _FlockAggregate &a) {
  
  //We want to get the distance between boids
  float d = glm::distance(b->current().position, _current.position);
  
  if ((d > 0) && (d < _weights->distance)) {
    a.sum += b->current().position;
    a.n++;
  }
  
}

glm::vec3 Boid::_calculateCohesion(const _FlockAggregate &a) {
  
  glm::vec3 force = glm::vec3(0.0f);
  glm::vec3 target = glm::vec3(0.0f);
  
  if (a.n > 0) {
    target = a.sum / a.n;
    glm::vec3 desired = target - _current.position;
    desired = glm::normalize(desired);
    desired *= MAX_VELOCITY;
    force = _limit(desired - _current.velocity, MAX_FORCE);
  }
  
  return force;
  
}

void Boid::_accumulateSeparation(Entity::Ptr b, _FlockAggregate &a) {
  
  //We want to get the distance between boids
  float d = glm::distance(b->current().position, _current.position);
  
  if ((d > 0) && (d < _weights->distance)) {
    glm::vec3 diff = b->current().position - _current.position;
    diff = glm::normalize(diff) / d;
    a.sum += diff;
    a.n++;
  }
  
}

glm::vec3 Boid::_calculateSeparation(const _FlockAggregate &a) {
  
  //note so far, this is exactly the same as align
  
  glm::vec3 force = glm::vec3(0.0f);
  glm::vec3 separate = glm::vec3(0.0f);
  
  
  if (a.n > 0) {
    separate = a.sum / (float)a.n;
    separate = glm::normalize(separate);
    separate *= MAX_VELOCITY;
    //Calculate the steering force
    force = _limit(_current.velocity - separate, MAX_FORCE);
  }
  
  if (_id == TRACE_ID) {
    cout << "Boid::TRACE_ID::" << ofToString(_id) << " sum: " << a.sum << " n: " << a.n  << " separate: " << separate << " force: " << force << endl;
  }
  
  
  return force;
  
}

#pragma mark - Visibility

bool Boid::_boidIsVisible(Entity::Ptr b) {
  
  bool r = false;
  
  //First do a distance check
  float d2 = glm::distance2(b->current().position, _current.position);
  r = d2 < _weights->neighbourhood;
  
  //We need to calculate the angle to the specified boid,
  
  
  return r;
  
}



#pragma mark - General Force Related things

glm::vec3 Boid::_applyForce(glm::vec3 f) {
  
  return f / _props.mass;
  
}

glm::vec3 Boid::_limit(glm::vec3 v, float max) {
  
  glm::vec3 l = v;
  
  float mag = glm::length2(v);
  float max2 = max * max;
  
  if (mag > max2) {
    l = glm::normalize(l) * max;
  }
  
  return l;
  
}

bool Boid::_testOOB() {
  
  return !_bounds.test(_updated.position);
  
}



#pragma mark - Mesh Creation / Animation etc

void Boid::_makeMesh() {
  
  _mesh.addVertices({
    //triangle 1
    glm::vec3(0.0f),
    glm::vec3(-BOID_WIDTH, BOID_HEIGHT, BOID_BACK),
    glm::vec3(0.0f, 0.0f, BOID_LENGTH),
    //Triangle 2 (with 2 shared points to triangle 1)
    glm::vec3(BOID_WIDTH, BOID_HEIGHT, BOID_BACK)
  });
  
  _mesh.addColors({
    _props.color, _props.color, _props.color, _props.color
  });
  
  _mesh.addIndices({
    //triangle 1
    0, 1, 2,
    //trianfle 2
    0, 2, 3
  });
  
  
  _mesh.setMode(OF_PRIMITIVE_TRIANGLES);
  
}

void Boid::_animateMesh() {
  
  
  
}



#pragma mark - Audio playback

void Boid::fillBuffer(ofSoundBuffer& output, size_t size, uint64_t length, float gain) {
  
  if (_alive) {
    
    //Check if we need to do anything - four scenarios
    uint64_t now = ofGetElapsedTimeMicros();
    uint64_t end = now + length;
    
    //Determine range of samples to fill in this buffer
    size_t from;
    size_t to;
    
    
    //Cover 4 diff possibilities of gate status now and at end of the buffer sample period
    if (_gate.open) {
      //Gate Open
      if (_gate.next > end) {
        //is still open in length µs -> we fill the whole buffer
        from = 0;
        to = size;
      } else {
        //has closed in length µs
        from = 0;
        to = MIN(_usToSamples(_gate.next - now), size);             //MIN for safety in case the to is more than the nymber of samples
      }
    } else {
      //Gate Closed
      if (_gate.next > end) {
        //will not open in length µs
        //do nothing
        from = 0;
        to = 0;
      } else {
        //will open in length µs
        //fill buffer from when opens, and recall sample position
        from = MAX(_usToSamples(_gate.next - now), 0);      //MAX for safety, like the MIN above
        to = size;
      }
    }
    
    //Get samples from the wave table
    for (int i = from; i < to; i++) {
      double sample = _wtOsc.getSample(_frequency) * gain;
      _hoa.encodeSample(sample, i, _current.position, output);
    }

  }
  
}

size_t Boid::_usToSamples(uint64_t us) {
  
  //Convert a period of time in µs to samples, which requires the sample rate
  double sample = 1000000 / _sampleRate;
  double samples = us / sample;
  
  return (size_t)samples + 1;                 //assume we should round up to make sure we
  
}

double Boid::_midiNoteToFrequency(int note) {
  
  //Convert a MIDI note into a frequency, so we can play the wavetable
  //taken from https://www.music.mcgill.ca/~gary/307/week1/node28.html
  
  const double aFreq = 440;
  
  return (aFreq / 32) * pow(2, ((note - 9) / 12.0));
  
}
