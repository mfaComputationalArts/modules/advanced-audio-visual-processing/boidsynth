/**
 
 with-lasers.studio / lxinspc
 
 project name: BoidSynth
 file name: Boids.h
 
 created: 11/04/2022
 
 Collection of boid objects, which will fly around the scene, producing sound in space acoordingly
 
 Key features of the Boids collection
 
 1. To avoid unnecssary adjustment of memory when adding / destroying boids, we will always work with a fixed number of them - see the private const MAX_BOIDS to set this
 
 2. We implment the audio out functions of ofSoundStream here, as this is the obvious plave to loop through all the individual boids and get there position / necessary audio.
 
 3. As a result this is also where we implment a mutex - as the audioOut function will be called on a separate thread, and we therefore want some degree of memory safety betwene the threads, and we can hold up the update loop and make it wait for the audio loop to finish (as we do not want to disrupt the audio - as this will cause buffer issues, and sound quality issues)
 
 4. Will use this as an opportunity to explore
 
 Copyright © 2022 with-lasers.studio. All rights reserved.
 
 */

#pragma once

//openFrameworks includes
#include "ofMain.h"

//add on includes


//local project includes
#include "Boid.h"
#include "Octree.h"
#include "Bounds.h"
#include "WaveTables.h"

class Boids {
  
public:
  
  typedef shared_ptr<Boids> Ptr;
  
  Boids();
  
  //standard oF calls for upadte and draw
  void setup(Bounds bounds, WaveTables::Ptr waveTables);
  
  Bounds bounds();
  
  void addBoid(Boid::Properties p, glm::vec3 pos, glm::vec3 vel);
  
  
  void update();
  void draw();
  
  void clear();
  
  size_t size();
  
  //life cycle events for boids
  void createNew();
  
  
  //Audio out callback
  void audioOut(ofSoundBuffer& buffer);
  
private:
  
  Bounds _bounds;
  WaveTables::Ptr _waveTables;
  
  
  //Set an upper limit on the number of boids - this can help us in a couple of ways
  //1 - performance, we know we can't drive the synth beyond something it can't handle
  //2 - consistent memory allocation, and avoiding the need to delete unused boids
  //We will pay a small penalty by needing to loop over all of these - however as this is O(n) it's
  //probabaly worth it - and as we don't insert non alive boids into the octree structure, we avoid any uncessary
  //checks of boids against boids;
  const size_t MAX_BOIDS = 1000;
  
  //Primary vector of boids - reserved to the MAX_BOIDS size, and will use reallocation when new boids are created
  Boid::Boids _boids;

  //this stack tells us which boids are available for reuse
  stack<size_t> _reuseIndices;

  //Boid ID (will finish up being the same as it's index number)
  size_t _id = 0;

  Boid::WeightsPtr _weights;
  
  //We use an octree to optimise for boid neighbours avoiding the O(n2) penalty, replacing with I think O(logn)
  Octree::Ptr _octree;
  const size_t OCTREE_CAPACITY = 32;

  //Audio settings
  
  //Devices
  //We use Rogue Amoeba's Loopback to create a 16 channel device we can send sound to
  //this is then picked up in Max so that we can apply the chosen decoder (binaural for headphones
  //of IEM AIIRADecode for a space with multi-speaker setup
  const string DEVICE_NAME = "Rogue Amoeba Software, Inc.: Ambisonic3";

  //Other sound outputs which we might use during development
  //const string DEVICE_NAME = "Focusrite: Scarlett 2i4 USB";  //Use for stero output
  //const string DEVICE_NAME = "Rogue Amoeba Software, Inc.: Loopback Audio"; //loopback for audio recording
  
  //Device settings
  const size_t SAMPLE_RATE = 48000;
  const size_t NUMBER_OUT_CHANNELS = 16;         //16 for HOA, 2 for other options
  const size_t BUFFER_SIZE = 512;
  
  const size_t BUFFER_US = ((1000000.0 / SAMPLE_RATE) * BUFFER_SIZE) + 1;    //Buffer in MS
  
  //Gain for each boid - making an assumption here that no more than 1000 (a 1/5 of max) Boids are sounding at once
  //may need to tweak this, in case we get clipping, or possibly role it off as numbers increase
  const double BOID_GAIN = 0.000001;
  
  const size_t BUFFER_N = NUMBER_OUT_CHANNELS * BUFFER_SIZE;

  void _setupSoundStream();
  ofSoundStream _soundStream;

  //Mutex to avoid contention between the audio thread and thr main thread which will do all the boid calculation
  mutex _soundMutex;
  
};




