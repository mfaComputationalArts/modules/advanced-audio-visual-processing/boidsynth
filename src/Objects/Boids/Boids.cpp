/**

   project name: BoidSynth
   file name: Boids.cpp

   created: 11/04/2022

   Copyright © 2022 with-lasers.studio. All rights reserved.

*/

#include "Boids.h"


Boids::Boids() {


}

void Boids::setup(Bounds bounds, WaveTables::Ptr waveTables) {
  
  //Reserve the max capcity we have defined for the boids vector
  _boids.reserve(MAX_BOIDS);
  
  _bounds = bounds;
  _waveTables = waveTables;
  
  _weights = make_shared<Boid::Weights>();
  
  //Setup audio
  _setupSoundStream();
  
}

Bounds Boids::bounds() {
  
  return _bounds;
  
}

void Boids::addBoid(Boid::Properties p, glm::vec3 pos, glm::vec3 vel) {
  
  //When we add a boid, we will start by making new ones and pushing them back into the reserved vector
  //however as time passes we will hit the capcity of the _boids vector, at this point we want to start reusing
  //boids that have been expended. We should be able to walk a line of always having expended more boids than we
  //want to add - though we may nned to monitor this!
  if (_boids.size() < MAX_BOIDS) {
    //Still in creation phase, so we make new objects and push back
    //Create a boid - we also load with it a random wave table
    Boid::Ptr b = make_shared<Boid>(_id++, p, _weights, pos, vel, _bounds, _waveTables->getRandomWaveTable(), SAMPLE_RATE);
    _boids.push_back(b);
  } else {
    //Reuse mode - so we have to get an index off the stack and revive this boid
    if (_reuseIndices.size() > 0) {
      //we have things to reuse - so thats good, lets do that… get top of stack, and pop it off
      int i = _reuseIndices.top();
      _reuseIndices.pop();
      _boids[i]->revive(p, pos, vel, _waveTables->getRandomWaveTable());
    } else {
      //we're out of boids, so we probabaly want to log this… (statistics table)
    }
  }
}


void Boids::update() {

  _octree =  make_shared<Octree>(_bounds, OCTREE_CAPACITY, true);

  //Keep track of indices for deletion
  size_t i = 0;
  //First place all of the boids into an octree
  for (Boid::Boids::iterator it = _boids.begin(); it != _boids.end(); ++it, i++) {
    if ((*it)->alive()) {
      _octree->insert((*it));
    }
    //Death is not exactly the opposite of alive
    if ((*it)->died()) {
      _reuseIndices.push(i);
    }
  }

  //Now octree is fully built, we can call update on it, which will process each octree cell
  //with it's negohbours - note this will call update on each boid for us
  //because each cell in the octree interacts with only it's other cell members - we can do the commint we need on each
  //octree cell - so we do the required mutex locking here, as this should mean the mutex locks are minimal and short
  //it should mean we can interlace these updates OK. We may occasionally get some visual glitches, but we prioritise audio
  //over the visual
  _octree->update(_soundMutex);
    
}






void Boids::draw() {

  _octree->draw(true, false);
  
  for (Boid::Boids::iterator it = _boids.begin(); it != _boids.end(); ++it) {
    (*it)->draw();
  }

//  _octree.reset();
  
}

void Boids::clear() {
  
  //May need to do some sanity checking round this to make sure this is OK to do and we don't currently have
  //anything else going on
  _boids.clear();
  
}


size_t Boids::size() {
  
  if (_boids.size() < MAX_BOIDS) {
    //not filled the vector yet
    return _boids.size();
  } else {
    //vector full - reuseIndices constains number of spare boids
    return _boids.size() - _reuseIndices.size();
  }

}




#pragma mark - Audio Things

void Boids::_setupSoundStream() {
  
  //Try to setup the soundstream based on the specifed device name - if we can't find
  //it then we will stop the application
  
  //Get a list of all the devices available
  vector<ofSoundDevice> devices = _soundStream.getDeviceList();
  
  vector<ofSoundDevice>::iterator it = find_if(devices.begin(), devices.end(), [&](const ofSoundDevice& d){
    return d.name == DEVICE_NAME;
  });
  
  if (it == devices.end()) {

    cout << "Could not find device " << DEVICE_NAME << " check loop back is running and virtual device has the right name" << endl;

  } else {
    cout << "Found device " << DEVICE_NAME << " will connect and setup" << endl;

    ofSoundStreamSettings settings;
    settings.setOutDevice(*(it));
    settings.setOutListener(this);
    settings.sampleRate = SAMPLE_RATE;
    settings.numOutputChannels = NUMBER_OUT_CHANNELS;
    settings.bufferSize = BUFFER_SIZE;
    
    _soundStream.setup(settings);

  }

}

void Boids::audioOut(ofSoundBuffer &buffer) {
  
  //locak the mutex - so that the main thread is prevented from doing the commit
  _soundMutex.lock();
  //Loop through all the boids and fill the sample
  for (Boid::Boids::iterator it = _boids.begin(); it != _boids.end(); ++it) {
    (*it)->fillBuffer(buffer, BUFFER_SIZE, BUFFER_US, BOID_GAIN);
  }
  //Done, we can now unlock the mutex
  _soundMutex.unlock();
  
}
