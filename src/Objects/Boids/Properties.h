/**
 
 with-lasers.studio / lxinspc
 
 project name: BoidSynth
 file name: Properties.h
 
 created: 02/05/2022
 
 Emitter / Boid properties - these are the ones used by the emitters to
 generate boids
 
 Copyright © 2022 with-lasers.studio. All rights reserved.
 
 */

#pragma once

//openFrameworks includes
#include "ofMain.h"

//add on includes
#include "ofxDatGui.h"

//local project includes
#include "Property.h"


class Properties {
  
public:
  
  typedef shared_ptr<Properties> Ptr;
  
  //The mode determines how the emitter releases notes - how this is implmentewd is between this class
  //and the Emitters class - as some are easier to do within a single one, and others need a little more
  //coordination
  enum Mode {
    //Unision - all emitters emit the same note (with detune options)
    UNISON,
    //Monophonic, whilst the note is down this emitter will always release the same note
    MONO,
    //Monophonic Rotate, note emission is rotated the emitters
    MONO_ROTATE,
    //Emits all notes from a single emitter
    POLYPHONIC
  };
  
  
  Properties();

  void update(glm::vec3& guiPos);

  //Accessors
  Mode mode();
  Property<int> gate();
  Property<int> repeat();
  int delay(bool varOnly = false);
  float velocity();
  float dAzimuth();
  float dElevation();
  int lifetime();
  int detune();
  float mass();
  Property<int> number();
  
private:
  
  //Mode - as above
  Mode _mode;
  //Gate, how long the boid should play for when it does in ms;
  Property<int> _gate;
  //Repeat, time between notes sounding (applied to the boid - use delay to effect time between boid releases)
  Property<int> _repeat;
  //Delay - the delay between new boids being created whilst the emitter is running
  Property<int> _delay;
  //Velocity - overall velocity with variation
  Property<float> _velocity;
  //Drift - variation in the launch velocity direction - drift values are applied to the elevation/azimuth
  struct {
    Property<float> azimuth;
    Property<float> elevation;
  } _drift;
  //Lifetime - how long the boid runs for in MS
  Property<int> _lifetime;
  //Detune - for UNISON / POLYPHONIC modes a detune amount
  Property<int> _detune;
  //Mass
  Property<float> _mass;
  //Number to release at once
  Property<int> _number;
  
  struct {
    //datGUI object
    ofxDatGui* gui;
    
    //Mode
    ofxDatGuiButton* mode;
    
  } _gui;
  
  string _getModeLabelText();
  
  
};



