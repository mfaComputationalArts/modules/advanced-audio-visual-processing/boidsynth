/**

   with-lasers.studio / lxinspc

   project name: BoidSynth
   file name: Octree.h

   created: 15/04/2022

   Cell represents at division of space within cube shaped bounds
 
 
 
   

   Copyright © 2022 with-lasers.studio. All rights reserved.

*/

#pragma once

//openFrameworks includes
#include "ofMain.h"

//add on includes


//local project includes
#include "Bounds.h"
#include "Entity.h"

class Octree {
    
public:
  
  typedef shared_ptr<Octree> Ptr;
  typedef vector<Ptr> Octrees;

  Octree(Bounds b, size_t capacity, bool isRoot = false);

  void insert(Entity::Ptr entity);

  void update();
  void update(mutex& m);

  void commit();
  void commit(mutex& m);
  
  //Draw - not used in main functionality - but is a useful routine to draw each
  //cell as it has been divided
  void draw(bool recurse = false, bool entities = false);
  
  
private:
  
  bool _isRoot;
  
  Octree* _root;
  
  size_t _capacity;

  const size_t N_CHILDREN = 2 * 2 * 2;
  
  Bounds _bounds;
  Entity::Entities _entities;

  //Hierarchy of Cells
  Octrees _subtrees;

  //Flat structure of cell references - only ever populated in the parent cell
  Octrees _flat;
  
  void _split();
  void _add(Entity::Ptr entity);

  bool _commited = false;
  void _commit();
  
  //Drawing things - mainly for debugging and checking stuff
  enum Category {
    ROOT,
    SUBDIVIDE,
    ENTITIES,
    ENTITY
  };
  
  typedef map<Category, ofColor> _Colors;
  
  _Colors _colors = {
    { ROOT, ofColor::aliceBlue },
    { SUBDIVIDE, ofColor::aliceBlue },
    { ENTITIES, ofColor::aliceBlue },
    { ENTITY, ofColor::orangeRed }
  };
  
  ofMesh _box;
  ofMesh _entityMesh;
  
};




