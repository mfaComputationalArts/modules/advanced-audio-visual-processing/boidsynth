/**
 
 project name: BoidSynth
 file name: Octree.cpp
 
 created: 15/04/2022
 
 Copyright © 2022 with-lasers.studio. All rights reserved.
 
 */

#include "Octree.h"

//Constructor - creates a new cell with bounds B and capcity C
Octree::Octree(Bounds b, size_t capacity, bool isRoot) {
  
  
  _bounds = b;
  
  //  ofLogVerbose() << "CREATED octree with bounds " << _bounds;
  
  _capacity = capacity;
  
  _entities.reserve(_capacity);
  _subtrees.reserve(N_CHILDREN);
  
  _isRoot = isRoot;
  
  glm::vec3 size = _bounds.size();
  
  _box = _bounds.makeMesh();
  
  //Setup the mesh for debug drawing of the entities
  _entityMesh.setMode(OF_PRIMITIVE_POINTS);
  
  for (_Colors::iterator it = _colors.begin(); it != _colors.end(); ++it) {
    it->second.a = 30;
  }
  
  
  
}

//Insert an item into the cell, and if necessary split and push down to lower cells
void Octree::insert(Entity::Ptr entity) {
  
  //What we do depends on the current occupany of this cell, and if it has already been split
  
  if (!_subtrees.empty()) {
    //Cell has been split - so we need to direct to the correct child cell
    _add(entity);
  } else if (_entities.size() < _capacity) {
    //can insert into this cell
    _entities.push_back(entity);
    //Add to debug display mesh
    _entityMesh.addVertex(entity->position());
    _entityMesh.addColor(_colors[ENTITY]);
  } else {
    //About to go over capacity so we need to split and add
    _split();
    _add(entity);
  }
  
}


#pragma mark - update

/**
 there are two versions of update - for development of the boid system we have been using a seperate final commit
 loop where we call the commit function and let it walk the tree for us.
 For adding in audio, we have a version where we have access to the mutex used for interacting with the audio thread
 and we will try to lock this mutex and commit each octree cell
 */


void Octree::update() {
  
  //This is the separate commit / no mutex version - only updates data leaving updated data in it's own space
  
  //Tree walking update function - where we either perform an update on this octree cell (as it's not split), or we call the split cells
  if (_subtrees.empty()) {
    //Loop though the entities, and call update on them
    for (Entity::Entities::iterator it = _entities.begin(); it != _entities.end(); ++it) {
      (*it)->update(_entities);
    }
  } else {
    //loop through subtrees, recursivly calling this function on each one
    for (Octrees::iterator it = _subtrees.begin(); it != _subtrees.end(); ++it) {
      (*it)->update();
    }
  }
  
}

void Octree::update(mutex& m) {
  
  //This is the update and commit version, using a mutex to lock the commits
  //Tree walking update function - where we either perform an update on this octree cell (as it's not split), or we call the split cells
  if (_subtrees.empty()) {
    //Loop though the entities, and call update on them - this puts data into entity::_updated
    for (Entity::Entities::iterator it = _entities.begin(); it != _entities.end(); ++it) {
      (*it)->update(_entities);
    }
    if (m.try_lock()) {
      //we locked the mutex - so commit everything
      _commit();
      _commited = true;
      m.unlock();
    } else {
      //Clashed - couple of things we can try here, we could delay, and try again - if we have a buffer size of 64 at 48KHz then this is 1/48 * 64 ms = 1.33ms - this is versus targetting 30-60fps for the boid system to update - which is 16.67ms -> 33.33ms - so we will have audio callbacks between 12-24 times during this time!
      
      //1. We could up the buffer size
      //2. We could delay this thread by same 500us and try again
      //3. Or we could just try again at the end of the main loop, and if we fail a second time abandon the updated data

      //set commited to false, we'll pick up everything uncomitted on a final pass (maybe :) )
      _commited = false;
    }
  } else {
    //loop through subtrees, recursivly calling this function on each one
    for (Octrees::iterator it = _subtrees.begin(); it != _subtrees.end(); ++it) {
      //only difference from the no commit version is passing on the mutex to the child trees/cells
      (*it)->update(m);
    }
  }
  
}




void Octree::commit() {

  //This is the bulk commit - not one to use when we are working with audio
  //Tree walking update function - where we either perform an update on this octree cell (as it's not split), or we call the split cells
  if (_subtrees.empty()) {
    //Loop though the entities, and call update on them
    _commit();
  } else {
    //loop through subtrees, recursivly calling this function on each one
    for (Octrees::iterator it = _subtrees.begin(); it != _subtrees.end(); ++it) {
      (*it)->commit();
    }
  }
  
}


void Octree::commit(mutex& m) {
  
  //This version basically repeats the commit we carry out in update, trying to lock the mutex - except it's only looking
  //for things which are uncommited
  if (_subtrees.empty()) {
    //Actual commit stage on entries here
    if (!_commited) {
      if (m.try_lock()) {
        //we locked the mutex - so commit everything for this cell
        _commit();
        _commited = true;
        m.unlock();
      } else {
        //we probabaly want to log this here - even pass up a count maybe of uncomitted cells so we can continue to optimise
      }
    }
  } else {
    //Need to walk down to the subtrees and try there - passing the mutex on, so recurse with this version of the commit
    for (Octrees::iterator it = _subtrees.begin(); it != _subtrees.end(); ++it) {
      (*it)->commit(m);
    }
  }
  
}




void Octree::_commit() {
  
  //This is the actual commit for when we have entities
  for (Entity::Entities::iterator it = _entities.begin(); it != _entities.end(); ++it) {
    (*it)->commit();
  }

  
}





#pragma mark - draw


void Octree::draw(bool recurse, bool entities) {
  
  //Function to help with testing - this draws a wireframe box based on the domain, and a point where each entity is located
  //it's a recursive call, so we just call this on the parent node, and then everything should be called accordingly
  
  //If this octree has been dividided, then we don't draw anything, we just all the sub octrees draw
  //however if it's still unidivided, and therefore contains
  
  if (recurse) {
    if (_subtrees.empty()) {
      //we need to pick the right colour
      if (_isRoot) {
        ofSetColor(_colors[ROOT]);
      } else if (_entities.empty()) {
        ofSetColor(_colors[SUBDIVIDE]);
      } else {
        ofSetColor(_colors[ENTITIES]);
      }
      //Draw the wireframe cube
      _box.drawWireframe();
      
      //Now draw the entities
      if (entities) {
        for (Entity::Entities::iterator it = _entities.begin(); it != _entities.end(); ++it) {
          (*it)->draw();
        }
      }
    } else {
      //there are subtrees - so we draw these
      for (Octrees::iterator it = _subtrees.begin(); it != _subtrees.end(); ++it) {
        (*it)->draw(recurse, entities);
      }
    }
  } else {
    ofSetColor(_colors[ROOT]);
    _box.drawWireframe();
  }
  
  
  
}




#pragma mark - split / insert / add functionality

void Octree::_split() {
  
  ofLogVerbose() << "SPLIT octree with bounds " << _bounds;
  
  //Create child cells by splitting the bounds, and moving existing objects to the new children
  for (int i = 0; i < N_CHILDREN; i++) {
    //create a child cell with the split bounds
    Octree::Ptr o = make_shared<Octree>(_bounds.splitAtIndex(i), _capacity);
    _subtrees.push_back(o);
  }
  
  //Move entities that are in in this cell into the correct subcell
  for (Entity::Entities::iterator it = _entities.begin(); it != _entities.end(); ++it) {
    _add((*it));
  }
  
  //Clean up as we don't want the boids in this cell any more
  _entities.clear();
  
  //We should also clean up the entity mesh, and remove all points and vertices
  _entityMesh.clearVertices();
  _entityMesh.clearColors();
  
}

void Octree::_add(Entity::Ptr entity) {
  
  
  //Get the index of the subcell
  size_t i = _bounds.splitIndexForCoordinate(entity->position());
  
  if (i >= 0 && i < N_CHILDREN) {
    //then do the same insert (thus meaning any over capcity should ripple down)
    _subtrees[i]->insert(entity);
  }
  //  } else {
  //    ofLogNotice("Attempt to add to invalid index for child: " + ofToString(i));
  //  }
  
  
}

