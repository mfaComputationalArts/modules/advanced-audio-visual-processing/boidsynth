/**

   with-lasers.studio / lxinspc

   project name: BoidSynth
   file name: Entity.h

   created: 15/04/2022

   A simple parent class for entities which are to be contained in cells in an octree space organisation optimisation
 
   Provides acces to the minimum data required for the cell / octree to work (namely position)

   Copyright © 2022 with-lasers.studio. All rights reserved.

*/

#pragma once

//openFrameworks includes
#include "ofMain.h"

//add on includes


//local project includes


class Entity {
  
public:
  
  typedef shared_ptr<Entity> Ptr;
  
  //Collection of entities, used during the update
  typedef vector<Ptr> Entities;

  //Accessors

  struct SharedData {
    glm::vec3 position;
    glm::vec3 velocity;
    float heading;
  };
  
  glm::vec3 position();
  glm::vec3 velocity();
  
  SharedData current();
  SharedData updated();
  
  bool alive();
  bool died();
  
  //Virtual update method - this can then be called on any entity within the octree
  virtual void update(Entities& entities) = 0;

  void commit();

  size_t id();
  
  virtual void draw() = 0;
  
  
  
protected:
  
  size_t _id;
  
    
  
  SharedData _current;
  SharedData _updated;
  
  bool _alive = true;
  bool _death = false;
  

private:
  
  
};


