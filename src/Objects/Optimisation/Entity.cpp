/**
   
   project name: BoidSynth
   file name: Entity.cpp

   created: 15/04/2022

   Copyright © 2022 with-lasers.studio. All rights reserved.

*/

#include "Entity.h"

size_t Entity::id() {
  
  return _id;
  
}


glm::vec3 Entity::position() {
  
  return _current.position;
  
}

glm::vec3 Entity::velocity() {
  
  return _current.velocity;
  
}

Entity::SharedData Entity::current() {
  
  return _current;
  
}

Entity::SharedData Entity::updated() {
  
  return _updated;
  
}

bool Entity::alive() {
  
  return _alive;
  
}

bool Entity::died() {
  
  //You can only die once!
  bool r = _death;
  _death = false;
  return r;
  
}


void Entity::commit() {
  
  _current = _updated;
  
}


