//
//  Property.h
//  BoidSynth
//
//  Created by lxinspc on 30/04/2022.
//  Copyright © 2022 with-lasers.studio. All rights reserved.
//

#pragma once

#include "ofMain.h"


#include "ofxDatGui.h"

template <class T>
class Property {
public:
    
  //Default constructor
  Property() {}
  
  //Random constructor
  Property(T value, T variance) {
    set(value, variance);
  }
  
  //Signewd noise constructor
  Property(T value, T variance, float seed, float step) {
    set(value, variance, seed, step);
  }
  
  
  void set(T value, T variance) {
    _value = value;
    _variance = variance;
    _mode = PRNG;
  }
  
  //Full set of value / variance / seed and step
  void set(T value, T variance, float seed, float step) {
    _value = value;
    _variance = variance;
    _seed = seed;
    _step = step;
    _mode = NOISE;
  };
  
  /**
   Add GUI elements to an already existing DatGUI
   */
  void addGui(ofxDatGui* gui, string name, float min, float max, int precision) {
    
    //Folder
    _gui.folder = gui->addFolder(name);
    //Value
    _gui.value = _gui.folder->addSlider("value", min, max, _value);
    _gui.value->setPrecision(precision);
    _gui.value->bind(_value);
    //Variance
    _gui.variance = _gui.folder->addSlider("variance", min, max, _variance);
    _gui.variance->setPrecision(precision);
    _gui.variance->bind(_variance);
    //Expand folder
    _gui.folder->expand();
    
  }
  
  
  void setValue(T value) {
    _value = value;
  }
  
  void setVariance(T variance) {
    _value = variance;
  }
  
  void setSeed(float seed) {
    _seed = seed;
  }
  
  void setStep(float step) {
    _step = step;
  }
  
  T value() {
    return _value;
  }
  
  T variance() {
    return _variance;
  }
  
  T next() {
    return _mode == PRNG ? random() : noise();
  }
  
  T nextVariance() {
    return _mode == PRNG ? randomVariance() : noiseVariance();
  }
  
  //Get a value for the property using the PRNG
  T random() {
    return _value + (ofRandom(-1.0f, 1.0f) * _variance);
  }

  T randomVariance() {
    return ofRandom(1.0f) * _variance;
  }
  
  //Get a value for the property using signed noise
  T noise() {
    _seed += _step;
    return _value + (ofSignedNoise(_seed) * _variance);
  }
  
  T noiseVariance() {
    _seed += _step;
    return ofNoise(_seed) * _variance;
  }
  
private:
  
  enum Mode {
    PRNG,
    NOISE
  } _mode;
  
  T _value;
  T _variance;
  
  float _seed;
  float _step;
  
  //Gui controls
  struct Gui {
    ofxDatGuiFolder* folder;
    ofxDatGuiSlider* value;
    ofxDatGuiSlider* variance;
  } _gui;

  
};
