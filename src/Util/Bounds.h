/**

   with-lasers.studio / lxinspc

   project name: BoidSynth
   file name: Bounds.h

   created: 12/04/2022

   Some simple functions for representing a cube in 3D space - testing is a point is in / outside of the cube, and a
   factory function to return new bounds spliting the cube into 8

   Copyright © 2022 with-lasers.studio. All rights reserved.

*/

#pragma once

//openFrameworks includes
#include "ofMain.h"

//add on includes


//local project includes


class Bounds {
  
public:
 
  //Setup with a centre and size
  void set(glm::vec3 p, float size, int div = 2);
  //Setup with the two opposite corners of a cube diagonally - this will define the x/y/z min/max bounds of the cube
  void set(glm::vec3 p1, glm::vec3 p2, int div = 2);
  //check to see if a point lies within the bounds of this cube
  bool test(glm::vec3 p);
  
  glm::vec3 position();
  glm::vec3 size();
  
  ofRectangle xzRect();
  
  //return bounds split into 8 (i.e. 2 per dimension) based on the index number
  Bounds splitAtIndex(size_t i);
  size_t splitIndexForCoordinate(glm::vec3 p);

  glm::vec3 randomPositionWithin();
  
  string asString();
  
  ofMesh makeMesh();
  
  //Overload the << operator so we can use for easier output in messages
  friend ostream& operator<<(ostream& os, const Bounds& bounds);
  
  
private:
  
  glm::vec3 _p1;
  glm::vec3 _p2;

  int _div;               //Number of divisions by axis
  glm::vec3 _size;        //Split size
  
};





