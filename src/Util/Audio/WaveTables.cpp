/**
 
 project name: BoidSynth
 file name: WaveTables.cpp
 
 created: 03/05/2022
 
 
 
 */

#include "WaveTables.h"


WaveTables::WaveTables(int nBanks) {
  
  for (int i = 0; i < N_BANKS; i++) {
    //load banks
    string filename = "wavetables/wavetable" + ofToString(i + 1, 2, '0') + ".txt";
    _loadFile(filename, i);
  }
  
}

WaveTableOsc::WaveTablePtr WaveTables::getRandomWaveTable() {
  
  int bank = 0; //ofRandom(N_BANKS);
  int id = ofRandom(_banks[bank].size());
  
  return _banks[bank][id];
  
}



WaveTableOsc::WaveTablePtr WaveTables::getWaveTable(int bank, int id) {
  
  return _banks[bank][id];

}


void WaveTables::_loadFile(string filename, int bank) {
  
  ofFile _loadFile(filename);
  
  if (_loadFile.exists()) {
    
    ofBuffer buffer(_loadFile);
        
    int n = 0;
    
    int max = 0;
    
    for (ofBuffer::Line it = buffer.getLines().begin(); it != buffer.getLines().end(); ++it, n++) {
    
      WaveTableOsc::WaveTablePtr wt = make_shared<WaveTableOsc::WaveTable>();
    
      string line = *it;
      //Split at comma
      vector<string> values = ofSplitString(line, ",");
      
      max = MAX(max, values.size() - 1);
      
      int id;
      
      int i = 0;
      for (vector<string>::iterator vit = values.begin(); vit != values.end(); ++vit, i++) {
        if (i == 0) {
          id = ofToInt(*vit);
        } else {
          wt->push_back(ofToDouble(*vit));
        }
      }
      (*wt).push_back((*wt)[0]);
      (*wt).push_back((*wt)[1]);
      _banks[bank][id] = wt;
    }
    
    ofLogNotice("loaded file: " + filename + " with " + ofToString(n) + " wavetables " + ofToString(max) + " max samples ");
    
  } else {
    ofLogError("fielname " + filename + " does not exist");
  }
  
}
