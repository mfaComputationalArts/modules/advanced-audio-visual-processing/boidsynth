/**
 
 Class: WaveTableOsc

 Created as Maximilian didn't seem to have a waveTable implmentation - but does have a sine buffer one so…
 
 This is based on maxiOsc sinebuf4 which used 4 point interpolation on a buffer of a sinewave
 the difference here is that we can load out own data into the buffer.
 
 Taken from coursework assignment NoiseWavetables - but modified to use a shared_ptr to the
 wave table - so we can have an oscialtor for each boid, but not loads of copies of the wavetables
 
 */


#pragma once

//Openframeworks includes
#include "ofMain.h"

//Add on includes

//Local includes


class WaveTableOsc {
  
public:
  
  WaveTableOsc();
  
  typedef vector<double> WaveTable;
  typedef shared_ptr<WaveTable> WaveTablePtr;
  
  void setup(double sampleRate);
  
  //Set new wavetable data - addSamples used to add first two samples onto the end of wt
  void set(WaveTablePtr wt, bool addSamples = true);
 
  void resetPhase();
  
  //Get the next single sample from the WaveTable
  double getSample(double frequency);
  
  //Utility method for 4 point interpolation
  static double interpolate4(double a, double b, double c, double d, double remainder);
  
private:
  
  //Wave table size - defaults to 512, but is updated in setWaveTable
  double _size = 128;
  size_t _internal_size = _size + 2;
  
  int _sampleRate;
  
  //Keep track of current position
  double _phase;
  
  //The actual wavetable
  WaveTablePtr _waveTable;
  
};

