/**
   
   project name: BoidSynth
   file name: Bounds.cpp

   created: 12/04/2022

   Copyright © 2022 with-lasers.studio. All rights reserved.

*/

#include "Bounds.h"

void Bounds::set(glm::vec3 p, float size, int div) {
  
  
  _p1 = p - ( size / 2.0f);
  _p2 = p + ( size / 2.0f);
  
  _div = div;
  
  _size = glm::vec3(size);
  
}


void Bounds::set(glm::vec3 p1, glm::vec3 p2, int div) {
  
  _p1 = p1;
  _p2 = p2;

  _div = div;
  _size = _p2 - _p1;
  
}


bool Bounds::test(glm::vec3 p) {
  
  return p.x >= _p1.x && p.x < _p2.x && p.y >= _p1.y && p.y < _p2.y && p.z >= _p1.z && p.z < _p2.z;
  
}

glm::vec3 Bounds::position() {
  
  return _p1;
  
}

glm::vec3 Bounds::size() {
  
  return _size;
  
}

ofRectangle Bounds::xzRect() {
  
  return ofRectangle(0,0,_size.x, _size.z);
  
}


Bounds Bounds::splitAtIndex(size_t i) {
  
  glm::vec3 split = _size / _div;
  
  //Get X/Y/Z coordinate for index
  glm::vec3 p = glm::vec3(i % _div, i / _div % _div, i / (_div * _div));
  
//  cout << p.x << ", " << p.y << ", " << p.z << endl;
  
  glm::vec3 p1 = _p1 + (p * split);
  glm::vec3 p2 = p1 + split;
  
  Bounds r;
  r.set(p1, p2);
  
  return r;
  
}



size_t Bounds::splitIndexForCoordinate(glm::vec3 p) {
  
  //Need to locate the point within a split - we can get this by dividing relative position of p to p1 by
  //the divisor
  
  glm::vec3 split = _size / _div;
  
  int x = ( p.x - _p1.x ) / split.x;
  int y = ( p.y - _p1.y ) / split.y;
  int z = ( p.z - _p1.z ) / split.z;

  return ((( z * _div ) + y ) * _div ) + x;
  
}


glm::vec3 Bounds::randomPositionWithin() {
  
  glm::vec3 p = glm::vec3(ofRandom(1.0f), ofRandom(1.0f), ofRandom(1.0f));
  p *= _size;
  p += _p1;
  
  return p;
  
}


ofMesh Bounds::makeMesh() {
  
  ofMesh m;
  
  //make 8 vertices
  m.addVertex(_p1);
  m.addVertex(glm::vec3(_p2.x, _p1.y, _p1.z));
  m.addVertex(glm::vec3(_p2.x, _p2.y, _p1.z));
  m.addVertex(glm::vec3(_p1.x, _p2.y, _p1.z));
  m.addVertex(glm::vec3(_p1.x, _p1.y, _p2.z));
  m.addVertex(glm::vec3(_p2.x, _p1.y, _p2.z));
  m.addVertex(_p2);
  m.addVertex(glm::vec3(_p1.x, _p2.y, _p2.z));
  
  //indicies to draw the lines
  m.addIndices({
    0, 1,
    1, 2,
    2, 3,
    3, 0,
    3, 7,
    2, 6,
    1, 5,
    0, 4,
    4, 5,
    5, 6,
    6, 7,
    7, 4
  });
  
  //setr mode as lines
  m.setMode(OF_PRIMITIVE_LINES);
  
  return m;
  
}



ostream& operator<<(ostream& os, const Bounds& bounds) {

  string p1 = ofToString(bounds._p1.x, 2) + ", " + ofToString(bounds._p1.y, 2) + ", " + ofToString(bounds._p1.z);
  string p2 = ofToString(bounds._p2.x, 2) + ", " + ofToString(bounds._p2.y, 2) + ", " + ofToString(bounds._p2.z, 2);
  string size = ofToString(bounds._size.x, 2) + ", " + ofToString(bounds._size.y, 2) + ", " + ofToString(bounds._size.z, 2);

  os << "Bounds: p1: " << p1 << " p2: " << p2 << " size: " << size;
  return os;
  
}
