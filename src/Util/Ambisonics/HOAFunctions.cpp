//
//  HOAFunctions.cpp
//  loopbackOutput
//
//  Created by lxinspc on 25/03/2022.
//

#include "HOAFunctions.h"


void HOA::encodeSample(double v, size_t i, const Spherical& s, ofSoundBuffer& output) {
  
  size_t j = i * _nChannels;
  
  for (size_t k = 0; k < _nChannels; k++) {
    //Set gain for each channel
    output[j + k] += v * gainForChannel((int)k, s);
  }

}

void HOA::encodeSample(double v, size_t i, const glm::vec3& p, ofSoundBuffer& output) {
  
  size_t j = i * _nChannels;
  
  for (size_t k = 0; k < _nChannels; k++) {
    //Set gain for each channel
    output[j + k] += (v * gainForChannel((int)k, p) * 0.1);
  }

}

glm::vec3 HOA::sphericalToCartesian(const Spherical& s) {
  
  glm::vec3 r;
  
  r.x = s.r * cos(s.azimuth) * sin(s.elevation);
  r.y = s.r * sin(s.azimuth) * sin(s.elevation);
  r.z = s.r * cos(s.elevation);
  
  return r;
  
}



double HOA::gainForChannel(int c, const Spherical &s) {
  
  double r;
  
  switch (c) {
    case 0:
      //W
      r = 1.0;
      break;
    case 1:
      //Y
      r = sin(s.azimuth) * cos(s.elevation);
      break;
    case 2:
      //Z
      r = sin(s.elevation);
      break;
    case 3:
      //X
      r = cos(s.azimuth) * cos(s.elevation);
      break;
    case 4:
      //V
      r = ROOT_3_4 * sin( 2.0 * s.azimuth) * pow(cos(s.elevation), 2);
      break;
    case 5:
      //T
      r = ROOT_3_4 * sin( s.azimuth ) * sin( 2 * s.elevation);
      break;
    case 6:
      //R
      r = 0.5 * ( 3.0 * pow(sin(s.elevation ), 2) - 1.0);
      break;
    case 7:
      //S
      r = ROOT_3_4 * cos( s.azimuth ) * sin( 2.0 * s.elevation );
      break;
    case 8:
      //U
      r = ROOT_3_4 * cos( 2.0 * s.azimuth ) * pow(cos( s.elevation ), 2 );
      break;
    case 9:
      //Q
      r = ROOT_5_8 * sin( 3.0 * s.azimuth ) * pow(cos( s.elevation ), 3 );
      break;
    case 10:
      //O
      r = ROOT_15_4 * sin( 2.0 * s.azimuth ) * sin( s.elevation ) * pow( cos( s.elevation ), 2 );
      break;
    case 11:
      //M
      r = ROOT_3_8 * sin( s.azimuth ) * cos(s.elevation) * ( 5.0 * pow( sin ( s.elevation ), 2) - 1.0);
      break;
    case 12:
      //K
      r = 0.5 * sin( s.elevation ) * ( 5.0 * pow( sin( s.elevation ), 2) -3.0 );
      break;
    case 13:
      //L
      r = ROOT_3_8 * cos( s.azimuth ) * cos( s.elevation ) * ( 5.0 * pow( sin( s.elevation ), 2) - 1.0 );
      break;
    case 14:
      //N
      r = ROOT_15_4 * cos( 2.0 * s.azimuth ) * sin( s.elevation ) * pow( cos( s.elevation ), 2);
      break;
    case 15:
      //P
      r = ROOT_5_8 * cos( 3.0 * s.azimuth ) * pow( cos( s.elevation ), 3 );
      break;
    default:
      //not supported channel, return a gain of 0
      r = 0;
  }
  
  return r;
  
}

double HOA::gainForChannel(int c, const glm::vec3 &p) {
  
  //Openframeworks using the coordinate space where y & z are swapped with HOA conventions
  //so in the folloing we swap p.y for p.z and vice-a-versa
  
  float r;
  
  switch (c) {
      
    case 0:
      //W
      r = 1.0;
      break;
    case 1:
      //Y
      r = p.z; //SWAP
      break;
    case 2:
      //Z
      r = p.y; //SWAP
      break;
    case 3:
      //X
      r = p.x;
      break;
    case 4:
      //V
      r = ROOT_3 * p.x * p.z; //SWAP
      break;
    case 5:
      //T
      r = ROOT_3 * p.y * p.z; //SWAP (but no swap as p.y * p.z :) )
      break;
    case 6:
      //R
      r = 0.5 * ((3.0 * p.y * p.y) - 1.0);  //SWAP
      break;
    case 7:
      //S
      r = ROOT_3 * p.x * p.y;   //SWAP
      break;
    case 8:
      //U
      r = ROOT_3_4 * p.x * p.x * p.z * p.z;    //SWAP
      break;
    case 9:
      //Q - sqrt(5/8)*y*(3*x^2-y^2)
      r = ROOT_5_8 * p.z * ((3.0 * p.x * p.x - p.z * p.z));   //SWAP
      break;
    case 10:
      //O - sqrt(15)*x*y*z
      r = ROOT_15 * p.x * p.z * p.y;    //SWAP
      break;
    case 11:
      //M - sqrt(3/8)*y*(5*z^2-1)
      r = ROOT_3_8 * p.z * ((5.0 * p.y * p.y) - 1.0);    //SWAP
      break;
    case 12:
      //K - (1/2)*z*(5*z^2-3)
      r = 0.5 * p.y * ((5 * p.y * p.y) - 3.0);       //SWAP
      break;
    case 13:
      //L - sqrt(3/8)*x*(5*z^2-1)
      r = ROOT_3_8 * p.x * ((5 * p.y * p.y) - 1.0);      //SWAP
      break;
    case 14:
      //N - sqrt(15/4)*z*(x^2-y^2)
      r = ROOT_15_4 * p.y * (p.x * p.x - p.z * p.z);     //SWAP
      break;
    case 15:
      //P - sqrt(5/8)*x*(x^2-3*y^2)
      r = ROOT_5_8 * p.x * (p.x * p.x - 3 * p.z * p.z);   //SWAP
      break;
    default:
      r = 0.0;
  }
  
  
  
  
  return r;
  
  
}





