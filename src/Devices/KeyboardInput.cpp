/**
   
   project name: BoidSynth
   file name: KeyboardInput.cpp

   created: 01/05/2022

   Copyright © 2022 with-lasers.studio. All rights reserved.

*/

#include "KeyboardInput.h"

void KeyboardInput::setup(Emitters::Ptr emitters) {

  _emitters = emitters;
  
  //Not much to do here, except register the key down / up listeners for keyboard input
  ofAddListener(ofEvents().keyPressed, this, &KeyboardInput::keyPressed);
  ofAddListener(ofEvents().keyReleased, this, &KeyboardInput::keyReleased);

}


void KeyboardInput::keyPressed(ofKeyEventArgs &args) {
  
  if (args.key == PANIC) {
    //Panic forces a note off for everthing - there in case everything goes wrong
    _panic();
  } else {
    if (_modifierMap.count(args.key) > 0) {
      //modifier key was pressed
      _changeOctave(_modifierMap[args.key]);
    } else if (_noteMap.count(args.key) > 0) {
      _noteDown(_noteMap[args.key]);
    }
  }
  
}


void KeyboardInput::keyReleased(ofKeyEventArgs& args) {
  
  //Only interestewd in note keyups we can ignore panic and modifiers
  if (_noteMap.count(args.key) > 0) {
    _noteUp(_noteMap[args.key]);
  }
  
}


void KeyboardInput::_noteDown(int key) {
  
  int midiNote = _midiNoteFromKeyboardInput(key);
  //Sanity check, and make sure isn't down already
  if (_checkNote(midiNote) == -1) {
    _notePlaying.push_back(midiNote);
    //TODO: key velocity placeholder
    _emitters->noteOn(midiNote, 127);
//    cout << "NoteDown: " << midiNote << endl;
  }
  
}


void KeyboardInput::_noteUp(int key) {
  
  int midiNote = _midiNoteFromKeyboardInput(key);
  int i = _checkNote(midiNote);
  if (i >= 0) {
    _notePlaying.erase(_notePlaying.begin() + i);
    //TODO: call emitter to say note is up
    _emitters->noteOff(midiNote);
//    cout << "Noteup: " << midiNote << endl;
  }
  
}


void KeyboardInput::_panic() {
  
  //We want to clear all notes playing
  _notePlaying.clear();
  //TODO: call emitter to stop all notes playing
  _emitters->allOff();
//  cout << "Panic" << endl;
  
}


void KeyboardInput::_changeOctave(int modifier) {
  
  //Octave modifiers (via the z and x keys) - adjust octave value, and limit by LOW and HIGH
  //accordingly
  _currOctave += modifier;
  if (_currOctave < LOW_OCTAVE) {
    _currOctave = LOW_OCTAVE;
  }
  if (_currOctave > HIGH_OCTAVE) {
    _currOctave = HIGH_OCTAVE;
  }
//  cout << "octave: " << _currOctave << endl;
  
}

int KeyboardInput::_midiNoteFromKeyboardInput(int i) {
  
  //C0 is midi note 24 (treating as bottom of range)
  return 12 + (_currOctave * 12) + i;
  
}

int KeyboardInput::_checkNote(int midiNote) {
  
  //We return -1 if note isn't current playing
  int r = -1;
  
  //Check to see if the note is playing
  NotePlaying::iterator it = find_if(_notePlaying.begin(), _notePlaying.end(), [&](const int i) {
    return i == midiNote;
  });
  
  //Get the index of the note if it was found, so we can return this (used in noteUp)
  if (it != _notePlaying.end()) {
    r = (int)distance(_notePlaying.begin(), it);
  }
  
  return r;
  
}



