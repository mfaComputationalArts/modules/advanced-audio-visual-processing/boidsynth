/**

   with-lasers.studio / lxinspc

   project name: BoidSynth
   file name: KeyboardInput.h

   created: 01/05/2022

   Computer MIDI Keyboard:
 
   Same style as the Ableton Live one, where Z/X control octave, and the ASDF row of keys are the white notes, and QWER… are the black notes

   Copyright © 2022 with-lasers.studio. All rights reserved.

*/

#pragma once

//openFrameworks includes
#include "ofMain.h"

//add on includes


//local project includes
#include "Emitters.h"


class KeyboardInput {
  
public:
  
  void setup(Emitters::Ptr _emitters);
  
  //, (*keyUp(int)), (*panic(void))
  
  void keyPressed(ofKeyEventArgs& args);
  void keyReleased(ofKeyEventArgs& args);
  
private:
  
  //Emitters pointer for calling back to
  Emitters::Ptr _emitters;
  
  typedef map<int, int> KeyMappings;

  //Key mappings, use the same as abletons Computer MIDI keyboard
  KeyMappings _modifierMap = {
    //Octave Modifiers
    { 'z', - 1 },
    { 'x', + 1 }
  };

  KeyMappings _noteMap = {
    //Actual Keys
    { 'a', 0 },                          //C
    { 'w', 1 },                          //C# / D♭
    { 's', 2 },                          //D
    { 'e', 3 },                          //D# / E♭
    { 'd', 4 },                          //E
    { 'f', 5 },                          //F
    { 't', 6 },                          //F# / G♭
    { 'g', 7 },                          //G
    { 'y', 8 },                          //G# / A♭
    { 'h', 9 },                          //A
    { 'u', 10 },                         //A# / B♭
    { 'j', 11 },                         //B
    { 'k', 12 }                          //C
  };

  const int PANIC = 'p';
  
  
  void _noteDown(int key);
  void _noteUp(int key);

  void _panic();

  
  int _midiNoteFromKeyboardInput(int i);

  void _changeOctave(int modifier);
  
  int _currOctave = 2;                   //So we start a C4 - middle C

  const int LOW_OCTAVE = 0;
  const int HIGH_OCTAVE = 9;
  
  //Track which notes are playing, so we can better track when released
  typedef vector<int> NotePlaying;
  NotePlaying _notePlaying;

  int _checkNote(int midiNote);
  
};




